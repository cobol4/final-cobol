       IDENTIFICATION DIVISION.
       PROGRAM-ID. TRADER.
       AUTHOR. NATCHA.

       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT INPUT-FILE ASSIGN TO "trader4.dat"
           ORGANIZATION IS LINE SEQUENTIAL.
           
           SELECT OUTPUT-FILE ASSIGN TO "trader.rpt"
           ORGANIZATION IS LINE SEQUENTIAL.
       
       DATA DIVISION.
       FILE SECTION.
       FD  INPUT-FILE.
       01  INPUT-BUFFER.
           88 END-OF-INPUT-FILE VALUE HIGH-VALUES.
           05 PV-ID PIC X(2).
           05 TRADER-ID PIC X(4).
           05 INCOME PIC 9(6).
       FD  OUTPUT-FILE.
       01  OUTPUT-BUFFER.
           05 RPT-HEADER1.
              08 FILLER PIC X(14) VALUE "   PROVINCE   ".
              08 FILLER PIC X(20) VALUE "    P INCOME       ".
              08 FILLER PIC X(14) VALUE "   MEMBER  ".
              08 FILLER PIC X(20) VALUE "  MEMBER INCOME  ".
           05 RPT-DETAIL1.
              08 PRN-PV-ID1 PIC X(2).
              08 PRN-P-INCOME1 PIC $$$,$$$,$$9.
              08 PRN-MEMBER1 PIC X(4).
              08 PRN-MEMBER-INCOME1 PIC $$$,$$9.
              
       
       WORKING-STORAGE SECTION.
       01  PV-PROCESSING PIC X(2).
       01  P-INCOME PIC 9(9).
       
       01  RPT-HEADER.
           05 FILLER PIC X(14) VALUE "   PROVINCE   ".
           05 FILLER PIC X(20) VALUE "    P INCOME       ".
           05 FILLER PIC X(14) VALUE "   MEMBER  ".
           05 FILLER PIC X(20) VALUE "  MEMBER INCOME  ".
       01  RPT-DETAIL.
           05 PRN-PV-ID PIC X(2).
           05 PRN-P-INCOME PIC $$$,$$$,$$9.
           05 PRN-MEMBER PIC X(2).
           05 PRN-MEMBER-INCOME PIC $$$,$$9.

       PROCEDURE DIVISION.
       BEGIN.
           OPEN INPUT INPUT-FILE
           OPEN OUTPUT OUTPUT-FILE
           DISPLAY RPT-HEADER
           PERFORM READ-LINE
           PERFORM PROCESS-P-INCOME UNTIL END-OF-INPUT-FILE
      *    DISPLAY "HELLO"
      *    DISPLAY INPUT-BUFFER
           MOVE P-INCOME TO PRN-P-INCOME
           DISPLAY PRN-P-INCOME
           MOVE RPT-DETAIL TO RPT-DETAIL1
           WRITE OUTPUT-BUFFER
           CLOSE OUTPUT-FILE
           CLOSE INPUT-FILE
           GOBACK
           .
       
       PROCESS-P-INCOME.
           MOVE PV-ID TO PV-PROCESSING
           MOVE ZEROS TO P-INCOME
           IF PV-ID EQUAL TO PV-PROCESSING
           COMPUTE P-INCOME = P-INCOME + INCOME
              ELSE
                 DISPLAY "TEST"
                 PERFORM PRN-REPORT
           END-IF
      *    DISPLAY "P-INCOME = " P-INCOME
           .
       PROCESS-NEW-INCOME.
           COMPUTE P-INCOME = P-INCOME + INCOME
           .
       
       PRN-REPORT.
           MOVE PV-PROCESSING TO PRN-PV-ID
           MOVE P-INCOME TO PRN-P-INCOME
           MOVE TRADER-ID TO PRN-MEMBER
           MOVE INCOME TO PRN-MEMBER-INCOME
           .
       
       READ-LINE.
           READ INPUT-FILE 
              AT END SET END-OF-INPUT-FILE TO TRUE
           END-READ
           .
